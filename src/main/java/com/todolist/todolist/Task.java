package com.todolist.todolist;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id; 

    private String taskName;

    @ManyToOne //(fetch=FetchType.EAGER)
    private Week week;

    //@OneToMany(fetch=FetchType.EAGER)
    //@JoinColumn(name = "task")


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public Integer getWeek() {
        return week.getId();
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public void setWeek(Week week) {
        this.week = week;
    }

    // public List<Task> getTaskName() {
    //     return this.taskName
    // }
}
