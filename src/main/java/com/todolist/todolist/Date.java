package com.todolist.todolist;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Date {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String date;

    @ManyToOne
    private Task task;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
