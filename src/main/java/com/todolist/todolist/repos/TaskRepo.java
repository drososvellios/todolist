package com.todolist.todolist.repos;

import com.todolist.todolist.Task;

import org.springframework.data.jpa.repository.JpaRepository;

interface TaskRepo extends JpaRepository<Task, Integer>{
    
}
