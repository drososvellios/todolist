package com.todolist.todolist.repos;

import com.todolist.todolist.Date;

import org.springframework.data.jpa.repository.JpaRepository;

interface DateRepo extends JpaRepository<Date, Integer>{
    
}
