package com.todolist.todolist.repos;


import java.util.List;

import com.todolist.todolist.Task;
import com.todolist.todolist.Week;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TaskController {
    private WeekRepo weekRepo;
    private TaskRepo taskRepo;

    public TaskController(WeekRepo weekRepo, TaskRepo taskRepo) {
        this.weekRepo = weekRepo;
        this.taskRepo = taskRepo;
    }

    @GetMapping("/week/{week_id}/task")
    public List<Task> getTask(@PathVariable Integer week_id) {
        Week week = weekRepo.findById(week_id).get();
        return week.getTasks();    
    }  

    @PostMapping("/week/{week_id}/task")
    public Task createTask(@RequestBody Task taskData, @PathVariable Integer week_id) {
        Week week = weekRepo.findById(week_id).get();
        taskData.setWeek(week);
        return taskRepo.save(taskData);
        // taskData.setWeek(week);
        // return weekRepo.save(taskData);

        // return weekRepo.findById(week_id).get();

        // @PostMapping("/guitars/{guitar_id}/pickups")
        // public Guitar createPickups(@RequestBody Pickups pickupData, @PathVariable Integer guitar_id) {
        // Guitar guitar = guitarRepo.findById(guitar_id).get();
        // pickupData.setGuitar(guitar);
        // pickupsRepo.save(pickupData);
        // return guitar;
    }

    @PutMapping("/week/{week_id}/task/{id}")
    public Task updateTask(@PathVariable Integer week_id, @PathVariable Integer id) {
        Task task = taskRepo.findById(id).get();
        Week week = weekRepo.findById(week_id).get();
        task.setWeek(week);
        return taskRepo.save(task);
    }


    @DeleteMapping("/week/{week_id}/task/{id}")
    public void deleteOne(@PathVariable Integer id) {
        taskRepo.deleteById(id);
    }


 
}
