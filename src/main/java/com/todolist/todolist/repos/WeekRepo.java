package com.todolist.todolist.repos;

import com.todolist.todolist.Week;

import org.springframework.data.jpa.repository.JpaRepository;

interface WeekRepo extends JpaRepository<Week, Integer>{
    
}
