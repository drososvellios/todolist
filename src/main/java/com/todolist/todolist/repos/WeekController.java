package com.todolist.todolist.repos;

import java.util.List;

import com.todolist.todolist.Week;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeekController {
    private WeekRepo repo;

    public WeekController(WeekRepo repo) {
        this.repo=repo;
        List<Week> weeks = repo.findAll();
        if (weeks.size() == 0) {
            String[] names = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday", "Completed"};
            for (String name : names) {
                Week week = new Week();
                week.setName(name);
                this.repo.save(week);
            }
            
          //  repo.save(new Week("Monday"));
        }
    }

    @PostMapping("/week")
    public Week createWeek(@RequestBody Week weekData) {
        return repo.save(weekData);
    }

    @GetMapping("/week") 
    public List<Week> getAll() {
        return repo.findAll();
    }

    @GetMapping("/week/{id}") 
    public Week getOne(@PathVariable Integer id) {
        return repo.findById(id).get();
    }

    @PutMapping("/week/{id}")
    public Week updateOne(@RequestBody Week updateWeek, @PathVariable Integer id) {
        return repo.findById(id).map(week -> {
            week.setName(updateWeek.getName());
            return repo.save(week);
        }).orElseThrow();
    }

    @DeleteMapping("/week/{id}")
    public void updateOne(@PathVariable Integer id) {
        repo.deleteById(id);
    }
  }
