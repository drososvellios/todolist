let data = [];
const d = new Date();

let selected_task = [];
let weekDays = { Monday: "1", Tuesday: "2", Wednesday: "3", Thursday: "4", Friday: "5", Saturday: "6", Sunday: "7" }
let param = new URLSearchParams(window.location.search) 
let day = param.get("week") ? param.get("week") : d.getDay();

fetch(`/week/${day}/task`)
    .then(response => response.json())
    .then(_data => {
        data = _data
        renderTasks();
        console.log(data)
    })
    .catch(err => console.error(err))

const headerEl = document.getElementById('dayname')
headerEl.innerHTML = "Today's Tasks"

function renderTasks() {
    let content = data.map((taskData, i) => {
        return `
            <form id="complete" action=completeTask()>  
                <section>
                    <article> 
                        <input class="strikethrough" type="checkbox" id=${i} onChange="
                        const indexOf = selected_task.indexOf(${i})
                        if (indexOf < 0) { 
                            selected_task.push(${i})
                        } else {
                            selected_task.splice(indexOf, 1)
                        }
                        completeTask()  
                                           
                        "
                        name="task-name"> 
                        <label class="strikeThis" id=${taskData.taskName} for=${i}> ${taskData.taskName}</label>  
                        </article>  

                </section>
            </form>
            `
    }).join("")

    addTaskContent = getAddFormHtml();

    const taskEl = document.getElementById('home')
    taskEl.innerHTML = content

    const addTaskEl = document.getElementById('addTask')
    addTaskEl.innerHTML = addTaskContent
}

function getAllTasksForDay(dayIndex, day) {
    fetch(`/week/${dayIndex}/task`)
        .then(response => response.json())
        .then(_data => {
            data = _data
            renderTasks();
            const dayEl = document.getElementById('dayname')
            dayEl.innerHTML = day
        })
        .catch(err => console.error(err))
}

function getAddFormHtml() {
    return `   
        <article id="addNew"> <h1>Want to add a new task?</h1>
        <br>
            <form onsubmit="event.preventDefault();addTask(this);">
                <div>
                    <label for="days">Day</label>
                    <select id="dayName" name="dayName">
                        <option dayName="Monday">Monday</option>
                        <option dayName="Tuesday">Tuesday</option>
                        <option dayName="Wednesday">Wednesday</option>
                        <option dayName="Thursday">Thursday</option>
                        <option dayName="Friday">Friday</option>
                        <option dayName="Saturday">Saturday</option>
                        <option dayName="Sunday">Sunday</option>
                    <select/>
                    
                    <label>Task</label>
                    <input name="taskName" required />
                </div>
                <br>
                <button id="taskButton">Add Task</button>
             </form>
        </article>
    `
}

function addTask(HTMLform) {
    const form = new FormData(HTMLform)
    const taskName = form.get("taskName")
    const week_id = weekDays[form.get("dayName")]
    fetch(`/week/${week_id}/task`, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ taskName, week_id })
    })
        .then(res => res.json())
        .then(week => {
           //data.push(week)
           window.location.href=`?week=${week_id}`

        })
        .catch(console.error)
}

function completeTask() {
    let taskName
    //  for(const task of selected_task) {

    selected_task.forEach(function (taskIndex, index) {

        console.log(data[taskIndex])

        setTimeout(function () {
            taskName = data[taskIndex].taskName
            console.log(taskName)
            fetch(`/week/8/task/${data[taskIndex].id}`, {
                method: 'PUT'

            }).then(res => res.json())
            .then(function () {
                    /*
                    if (index === selected_task.length-1) {
                        deleteCompletedTasks();
                    }
                    */
                   data.splice(taskIndex,1);

                    renderTasks()
                })
                .catch(console.error)
        }, 1000 * index)
    })

}

function deleteCompletedTasks() {
    selected_task.forEach(function (taskIndex, index) {
        setTimeout(function () {
            try {
                console.log(data)
                const id = data[taskIndex].id

                fetch(`/week/_/task/${id}`, {
                    method: 'DELETE'
                })
                    .then(function () {
                        selected_task.splice(index, 1);
                        data.splice(taskIndex, 1);
                    })
                    .catch(console.error)
            } catch (error) {
                console.error(error)
            }
        }, 1000 * index)
    })
    renderTasks()
}


function deleteTaskFromDay() {
    let taskName = selected_task[0]
}